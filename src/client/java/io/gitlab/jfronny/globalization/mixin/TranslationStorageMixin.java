package io.gitlab.jfronny.globalization.mixin;

import com.llamalad7.mixinextras.sugar.Local;
import io.gitlab.jfronny.globalization.GlobalizationMap;
import net.minecraft.client.resource.language.TranslationStorage;
import net.minecraft.resource.ResourceManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.*;

@Mixin(TranslationStorage.class)
public class TranslationStorageMixin {
    @ModifyVariable(method = "load(Lnet/minecraft/resource/ResourceManager;Ljava/util/List;Z)Lnet/minecraft/client/resource/language/TranslationStorage;", at = @At("STORE"), index = 3)
    private static Map<String, String> globalization$createCustomMap(Map<String, String> original) {
        if (!original.isEmpty()) throw new IllegalStateException("Non-empty original");
        return new GlobalizationMap();
    }

    @Inject(method = "load(Lnet/minecraft/resource/ResourceManager;Ljava/util/List;Z)Lnet/minecraft/client/resource/language/TranslationStorage;", at = @At(value = "INVOKE", target = "Ljava/lang/String;format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;"))
    private static void globalization$forkCustomMap(ResourceManager manager, List<String> definitions, boolean leftToRight, CallbackInfoReturnable<String> cir, @Local Map<String, String> map) {
        globalization$getMap(map).fork();
    }

    @Redirect(method = "load(Lnet/minecraft/resource/ResourceManager;Ljava/util/List;Z)Lnet/minecraft/client/resource/language/TranslationStorage;", at = @At(value = "INVOKE", target = "Ljava/util/Map;copyOf(Ljava/util/Map;)Ljava/util/Map;", remap = false))
    private static Map<String, String> globalization$generateMissing(Map<String, String> source) {
        return Map.copyOf(globalization$getMap(source).generateMissing());
    }

    @Unique
    private static GlobalizationMap globalization$getMap(Map<String, String> map) {
        return (GlobalizationMap) map;
    }
}
