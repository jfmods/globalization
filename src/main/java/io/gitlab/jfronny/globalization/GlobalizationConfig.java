package io.gitlab.jfronny.globalization;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig(referencedConfigs = "libjf-translate-v1")
public class GlobalizationConfig {
    @Entry public static String targetCode = "en";

    static {
        JFC_GlobalizationConfig.ensureInitialized();
    }
}
