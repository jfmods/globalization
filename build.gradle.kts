plugins {
    id("jfmod") version "1.6-SNAPSHOT"
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "globalization"

jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.110.5+1.21.4"

    modrinth {
        projectId = "globalization"
        requiredDependencies.add("libjf")
        optionalDependencies.add("modmenu")
    }

    curseforge {
        projectId = "839010"
        requiredDependencies.add("libjf")
        optionalDependencies.add("modmenu")
    }
}

dependencies {
    modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v2")
    modImplementation("io.gitlab.jfronny.libjf:libjf-translate-v1")

    // Dev env
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-config-ui-tiny")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil")
    modLocalRuntime("com.terraformersmc:modmenu:12.0.0-beta.1")
    // for modmenu
    modLocalRuntime("net.fabricmc.fabric-api:fabric-resource-loader-v0")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-screen-api-v1")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-key-binding-api-v1")
}
