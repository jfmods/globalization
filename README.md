Have you ever tried playing a mod pack in a language other than English, only to find out half of your mods had no translations?

Globalization allows you to avoid that problem by automatically generating missing translations using libjf-translate.

To set the mod up, simply install it, configure it to target the correct language, then restart the game.

All translations will be logged, so if the game seems to be stuck, just look at your launchers log.